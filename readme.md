# AVR Dotstar SPI library

avr-gcc library for controlling dotstar leds using the SPI peripheral in:

* tinyAVR 0-series
* tinyAVR 1-series
* tinyAVR 2-series
* megaAVR 0-series
* AVR Dx and Ex devices, and probably future devices as well

Dotstars are receive-only SPI slaves without chip select lines, so it is very easy to handle them with the SPI peripheral

## How to use

1. Add this repo as a git submodule or download to your project folder
2. Copy [config/dotstar_config.h](config/dotstar_config.h) to your project and configure for your pinout and type of LEDs
3. Add the four ```dotstar``` source files to the project.
    Microchip Studio does not have an "add existing folder" selection, so the workaround is to click Show All Files and then include the files: ![Explorer Setup 1](example/studio_explorer_include.png "Solution Explorer - Show All Files - Include in Project")
4. Add the location of the config file and the location of the library to the compiler include directories.
    In Microchip Studio it is done like this: ![Toolchain Setup](example/studio_dirs.png "Toolchain - AVR/GNU C Compiler - Directories")
5. Include the library in your project files like this:

    ```c
    #include <dotstar.h>
    ```

See [dotstar_example.c](example/dotstar_example.c) for an example of how to use the driver.

### Interrupts

A big benefit of using dotstars over neopixels, other than speed, is that the timing between bytes transferred is not important.
This means that even if this driver is sending data in polling mode there is no problem at all to have interrupt routines disturb the data transfer to the LEDs. 
