/*
 * dotstar.h
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef DOTSTAR_H_
#define DOTSTAR_H_

/*
 * Includes
 */
#include "dotstar_spi.h"

/*
 * Redefines of functions to allow easy switch between drivers
 */
#define dotstar_init							dotstar_spi_init
#define dotstar_write_start						dotstar_spi_write_start				
#define dotstar_write_end						dotstar_spi_write_end
#define dotstar_write_single					dotstar_spi_write_single
#define dotstar_write_array						dotstar_spi_write_array
#define dotstar_write_constant					dotstar_spi_write_constant
#define dotstar_configure_neopixel_rgb_array	dotstar_spi_configure_neopixel_rgb_array

/*
 * Macros for easier config of dotstars
 */
#define dotstar_color(red, green, blue, intensity)	((color_t) {.r=red, .g=green, .b=blue, .brightness=intensity, .ones=0x7})
#define dotstar_off()								dotstar_color(0, 0, 0, 0)

/*
 * Defines for the hue-saturation-brightness functions
 */
#define HSB_MAX_HUE			1536

/*
 * Data type for the hue functions
 */
typedef struct {
	uint16_t h;
	uint8_t s;
	uint8_t b;
} hsb_t;

/*
 * Public functions
 */

void dotstar_configure_single(color_t color);
void dotstar_configure_constant(color_t color, uint16_t length);
void dotstar_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t length);
void dotstar_configure_array(color_t *array, uint16_t length);
void dotstar_configure_off(uint16_t length);
void dotstar_configure_off_and_single(color_t color, uint16_t position, uint16_t length);

color_t hsb2rgb(hsb_t config, uint8_t global_brightness);
color_t rotate_pixel_hue(uint8_t value, uint8_t brightness);

#endif /* DOTSTAR_H_ */
