/*
 * dotstar_spi.c
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */

#include <avr/io.h>
#include <stdint.h>
#include "dotstar_spi.h"

/*
 * Local defines
 */


/*
 * Private functions
 */
static void dotstar_spi_send_byte(uint8_t byte)
{
	while(!(LED_SPI.INTFLAGS & SPI_DREIF_bm)) {
		// Wait for empty buffer
	}

	LED_SPI.DATA = byte;
}

/*
 * Public functions
 */
void dotstar_spi_init(uint8_t prescaling)
{
	LED_SPI.CTRLB = SPI_BUFEN_bm | SPI_SSD_bm | SPI_MODE_0_gc;
	
	// In case you have a lot of LEDs in the string and running at high F_CPU
	if (prescaling <= 2)
	{
		LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV4_gc | SPI_ENABLE_bm | SPI_CLK2X_bm;
	} 
	else if (prescaling <= 4)
	{
		LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV4_gc | SPI_ENABLE_bm;
	} 
	else if (prescaling <= 8)
	{
		LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV16_gc | SPI_ENABLE_bm | SPI_CLK2X_bm;
	}
	else // I see no need to have more than 16x prescaler
	{
		LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV16_gc | SPI_ENABLE_bm;
	} 

	// Update PORTMUX to alternate SPI pinout, if used
	#if defined(PORTMUX_CTRLB) // Used by tiny 0- and 1-series
		PORTMUX.CTRLB |= LED_SPI_PORT_ALT;
	#elif defined(PORTMUX_TWISPIROUTEA) // Used by mega 0-series
		PORTMUX.TWISPIROUTEA |= LED_SPI_PORT_ALT;
	#elif defined(PORTMUX_SPIROUTEA) // Used by tiny 2-series and AVR Dx++
		PORTMUX.SPIROUTEA |= LED_SPI_PORT_ALT;
	#endif
	
	// Set MOSI and SCK to output
	LED_SPI_PORT.OUTCLR = LED_DATA_PIN | LED_SCK_PIN;
	LED_SPI_PORT.DIRSET = LED_DATA_PIN | LED_SCK_PIN;
}

void dotstar_spi_write_start(void) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES; Byte_Count > 0; Byte_Count--) {
		dotstar_spi_send_byte(0x00);
	}	
}

void dotstar_spi_write_end(uint16_t total_length) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES+(total_length/16)+1; Byte_Count > 0; Byte_Count--) {
		dotstar_spi_send_byte(0x00);
	}

	while(!(LED_SPI.INTFLAGS & SPI_TXCIF_bm));
	LED_SPI.INTFLAGS = SPI_TXCIF_bm;
}

void dotstar_spi_write_single(color_t color)
{
	for(uint8_t Byte_Count = 0; Byte_Count < DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar_spi_send_byte(color.array[Byte_Count]);
	}
}

void dotstar_spi_write_array(color_t *array, uint16_t length)
{
	uint8_t *LED_Bytes = (uint8_t *) array;
	for(uint16_t Byte_Count = 0; Byte_Count < length*DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar_spi_send_byte(LED_Bytes[Byte_Count]);
	}
}

void dotstar_spi_write_constant(color_t color, uint16_t length)
{
	while(length--) {
		dotstar_spi_write_single(color);
	}
}

void dotstar_spi_configure_neopixel_rgb_array(uint24_t *array, uint16_t length, uint8_t brightness)
{
	brightness |= (0x7 << 5); // Top 3 bits always 1
	uint8_t *LED_Bytes = (uint8_t *) array;
	dotstar_spi_write_start();
	for(uint16_t count = 0; count < length; count++) {
		dotstar_spi_send_byte(brightness);
		dotstar_spi_send_byte(*LED_Bytes++);
		dotstar_spi_send_byte(*LED_Bytes++);
		dotstar_spi_send_byte(*LED_Bytes++);
	}
	dotstar_spi_write_end(length);
}
